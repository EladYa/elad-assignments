import sys
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import cv2

nMonte = 10000

def dice(n = None):
    return np.random.choice(range(1,7), n, replace=True)

def coin(n = None):
    return np.random.choice([0,1], n, replace = True)

def binomial(test, value, k, printRes = False):
    """
    Return number of x = value cases over k tests for test function test
    :param test: function (n)
    a function that returns a value in n independent tests
    :param value: any
    a desired value to test
    :param k: int
    number of tests
    :return: int
    number of times x == value
    """
    results = test(k)
    if printRes:
        print results
    return (np.count_nonzero(results == value))

def countWord(interval, word):
    res = 0
    for w in interval:
        if w == word:
            res += 1
    return res

def wordInText(text, word, k, printRes = False): #I hope it is similar to Poisson for some cases
    """
    Takes a random k word interval in text and returns how many occurrences of the word 'word' happened
    :param text: list [string]
    list of words
    :param word: string
    :param k: int
    word interval length
    :return: int
    number of appearences
    """
    N = len(text)
    n0 = np.random.choice(N-k)
    interval = text[n0:n0+k]
    if printRes:
        print(interval)
    res = countWord(interval, word)
    return res

def meanWordLen(text, k):
    """
    returns the mean word length of a random k word interval in a text
    :param text: list [string]
    list of words
    :param word: string
    :param k: int
    word interval length
    :return: float
    average word length of a sentence
    """
    N = len(text)
    n0 = np.random.choice(N - k)
    interval = text[n0:n0 + k]

    return np.mean([len(word) for word in interval])

def binomialFromImage(img, threshold, k):
    """
    samples k random pixels in image and returns hoe many of them are above threshold
    :param img: ndarray
    :param threshold: float
    :param k: int
    :return: int
    number of successes
    """
    return np.count_nonzero(np.random.choice(img.reshape(-1), k) > threshold)

def CDF(func, integer = False):
    """
    Calculates cummulative distribution function
    :param func:
    :param integer: int
    whether the probability for non integer is zero
    :return: (ndarray, ndarray)
    x,y vales of CFD, axes if show == True
    """
    res = np.asarray([func() for _ in range(nMonte)])
    if integer:
        x = range(res.min(), res.max()+1)
    else:
        x = np.linspace(res.min(), res.max())
    y = np.asarray([np.count_nonzero(res < x0)*1. / nMonte for x0 in x])

    return(x,y)



def PDF(func, integer = False):
    """
    Calculates probability distribution function
    :param func:
    :param integer: int
    whether the probability for non integer is zero
    :return: (ndarray, ndarray)
    x,y vales of CFD
    """
    x, cdf = CDF(func, integer= integer)
    y = np.diff(cdf)
    y = np.concatenate((y, [1-cdf[-1]]))

    return(x,y)

def expectation(func):
    val = np.asarray([func() for _ in range(nMonte)])
    return val.mean()

def var(func):
    val = np.asarray([func() for _ in range(nMonte)])
    return val.std()**2


def parseTextFile(filename):
    """
    parses text file to sentences and words
    :param filename: string
    :return: (list[string], list[string]
    sentences, words
    """
    with open(filename, 'r') as myfile:
        text = myfile.read()
        text.replace('\r\n', ' ')
        words = text.split()
        sens = ' '.join(words).split('.')
    return sens, words

def jointPDFExample(words, word, k):
    """
    Calculates joint PDF for a k word interval in words. X1 is the number of the word `word' in the interval, X2 is the average word length
    :param words: list[string]
    :param word: string
    :param k: int
    :return: (x1, x2, y)
    """
    N = len(words)
    intervals = [words[n0:n0 + k] for n0 in np.random.choice(N - k, nMonte, replace=True)]
    x1, x2 = zip(*[(countWord(interval, word), np.mean([len(word) for word in interval])) for interval in intervals])
    H = np.histogram2d(x1, x2)
    return H




if __name__ == '__main__':
    #prepare image and text source
    sens, words = parseTextFile('book.txt')

    img = cv2.imread('img.jpg')
    img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)

    #poisson distribution CDF
    x, y = CDF(lambda : wordInText(words, 'the', 100), integer = True)
    fig = plt.figure()
    axes = fig.add_axes([0.1, 0.1, 0.8, 0.8])
    axes.scatter(x, y)
    axes.set_title('CDF function for number of occurrences of the word `the\' in 100 word intervals (Poisson)')
    plt.show()

    #Normal distribution
    x, y = PDF(lambda : meanWordLen(words, 100))
    fig = plt.figure()
    axes = fig.add_axes([0.1, 0.1, 0.8, 0.8])
    axes.plot(x, y, 'r')
    axes.set_title('PDF function for average word length in a 100 word interval (Gaussian)')
    plt.show()

    #Expectation
    print ('expectatoin for number of sixes in 100 dice rolls = {}'.format(expectation(lambda : binomial(dice, 6, 100))))

    #joind PDF. I calculated the joint probability of number of 'the' and average word length in a 100 word interval. Do,t see much correlation
    H, xedges, yedges = jointPDFExample(words, 'the', 100)
    H=H.T
    fig = plt.figure()
    axes = fig.add_subplot(111, title='Joint PDF for a 100 word interval. X1 is the number of appearences of the word \'the\', X2 is the average word length', aspect = 'equal', xlim = xedges[[0, -1]], ylim = yedges[[0, -1]])
    #axes = fig.add_axes([0.1, 0.1, 0.8, 0.8])
    im = mpl.image.NonUniformImage(axes, interpolation='bilinear')
    xcenters = (xedges[:-1] + xedges[1:]) / 2
    ycenters = (yedges[:-1] + yedges[1:]) / 2
    im.set_data(xcenters, ycenters, H)
    axes.images.append(im)
    plt.show()
