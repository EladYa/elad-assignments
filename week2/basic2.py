import sys
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import cv2

nMonte = 10000

#basic functions:
def parseTextFile(filename):
    """
    parses text file to sentences and words
    :param filename: string
    :return: list[string]
    words
    """
    with open(filename, 'r') as myfile:
        text = myfile.read()
        text.replace('\r\n', ' ')
        words = text.split()
    return words

def dice(n = None):
    return np.random.choice(range(1,7), n, replace=True)

def coin(n = None):
    return np.random.choice([0,1], n, replace = True)

def intervalFromText(text, k):
    """
    generates random k-length intervals from text
    :param text: list[string]
    list of words
    :param k: int
    :return: list[strings]
    """
    n0 = 0
    while n0+k <=len(text):
        yield(text[n0:n0+k])
        n0+=k

def countWord(interval, word):
    """
    returns how many times a word appears in an interval
    :param interval:
    :param word:
    :return:
    """
    res = 0
    for w in interval:
        if w == word:
            res += 1
    return res

def meanWordLen(interval):
    """
    returns the mean word length in an interval
    :param interval: list [string]
    list of words
    :return: float
    average word length of a sentence
    """
    return np.mean([len(word) for word in interval])


#binomials
def binomial(N):
    """
    generates N binomial distirbuted variables from a dice rolled 10 times
    :param N:
    :return:
    """
    n=0
    while n<N:
        results = dice(10)
        yield (np.count_nonzero(results == 3))
        n+=1

def binomialFromText(text, N):
    """
    generates N binomial variables from text
    :param text: list[string]
    :param N:
    :return:
    """
    for i, interval in enumerate(intervalFromText(text, 100)):
        if i >= N:
            break
        res = 0
        #count number of words without letter e
        for word in interval:
            if not 'e' in word:
                res+=1
        yield res

#Poisson
def poisson(N):
    """
    generates N poisson distributed variables from dice rolls. http://www.oxfordmathcenter.com/drupal7/node/297
    :param N:
    :return:
    """
    n = 0
    while n<N:
        rolls = [dice[3] for _ in range(100)]
        #count number of '1 1 1' rolls
        tripleOnes = np.asarray([roll == [1, 1, 1] for roll in rolls])
        yield np.sum(tripleOnes)
        n+=1

def poissonFromText(text, N):
    """
    generates N poisson distributed variables from text. http://www.oxfordmathcenter.com/drupal7/node/297
    :param text: list of words
    :param N:
    :return:
    """
    for i, interval in enumerate(intervalFromText(text, 100)):
        if i >= N:
            break
        #count number of 'the' in the interval
        yield countWord(interval, 'the')

#normal distribution
def normal(N):
    """
    generates N normal distributed numbers
    :param N:
    :return:
    """
    n = 0
    while n<N:
        results = dice(100)
        yield (np.count_nonzero(results == 3)/100.) #central limit theorem
        n+=1

def normalFromText(text, N):
    for i, interval in enumerate(intervalFromText(text, 100)):
        if i >= N:
            break
        #count number of 'the' in the interval
        yield meanWordLen(interval)

def CDF(func, integer = False):
    """
    Calculates cummulative distribution function
    :param func:
    :param integer: int
    whether the probability for non integer is zero
    :return: (ndarray, ndarray)
    x,y vales of CFD, axes if show == True
    """
    res = np.asarray([func() for _ in range(nMonte)])
    if integer:
        x = range(res.min(), res.max()+1)
    else:
        x = np.linspace(res.min(), res.max())
    y = np.asarray([np.count_nonzero(res < x0)*1. / nMonte for x0 in x])

    return(x,y)



def PDF(func, integer = False):
    """
    Calculates probability distribution function
    :param func:
    :param integer: int
    whether the probability for non integer is zero
    :return: (ndarray, ndarray)
    x,y vales of CFD
    """
    x, cdf = CDF(func, integer= integer)
    y = np.diff(cdf)
    y = np.concatenate((y, [1-cdf[-1]]))

    return(x,y)

def expectation(func):
    val = np.asarray([func() for _ in range(nMonte)])
    return val.mean()

def var(func):
    val = np.asarray([func() for _ in range(nMonte)])
    return val.std()**2




def jointPDFExample(words, word, k):
    """
    Calculates joint PDF for a k word interval in words. X1 is the number of the word `word' in the interval, X2 is the average word length
    :param words: list[string]
    :param word: string
    :param k: int
    :return: (x1, x2, y)
    """
    N = len(words)
    intervals = [words[n0:n0 + k] for n0 in np.random.choice(N - k, nMonte, replace=True)]
    x1, x2 = zip(*[(countWord(interval, word), np.mean([len(word) for word in interval])) for interval in intervals])
    H = np.histogram2d(x1, x2)
    return H




if __name__ == '__main__':
    #prepare image and text source
    words = parseTextFile('book.txt')

    i=0
    for n in normalFromText(words, 100):
        i+=1
        print(n)
    print(i)
    exit(0)

    img = cv2.imread('img.jpg')
    img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)

    #poisson distribution CDF
    x, y = CDF(lambda : wordInText(words, 'the', 100), integer = True)
    fig = plt.figure()
    axes = fig.add_axes([0.1, 0.1, 0.8, 0.8])
    axes.scatter(x, y)
    axes.set_title('CDF function for number of occurrences of the word `the\' in 100 word intervals (Poisson)')
    plt.show()

    #Normal distribution
    x, y = PDF(lambda : meanWordLen(words, 100))
    fig = plt.figure()
    axes = fig.add_axes([0.1, 0.1, 0.8, 0.8])
    axes.plot(x, y, 'r')
    axes.set_title('PDF function for average word length in a 100 word interval (Gaussian)')
    plt.show()

    #Expectation
    print ('expectatoin for number of sixes in 100 dice rolls = {}'.format(expectation(lambda : binomial(dice, 6, 100))))

    #joind PDF. I calculated the joint probability of number of 'the' and average word length in a 100 word interval. Do,t see much correlation
    H, xedges, yedges = jointPDFExample(words, 'the', 100)
    H=H.T
    fig = plt.figure()
    axes = fig.add_subplot(111, title='Joint PDF for a 100 word interval. X1 is the number of appearences of the word \'the\', X2 is the average word length', aspect = 'equal', xlim = xedges[[0, -1]], ylim = yedges[[0, -1]])
    #axes = fig.add_axes([0.1, 0.1, 0.8, 0.8])
    im = mpl.image.NonUniformImage(axes, interpolation='bilinear')
    xcenters = (xedges[:-1] + xedges[1:]) / 2
    ycenters = (yedges[:-1] + yedges[1:]) / 2
    im.set_data(xcenters, ycenters, H)
    axes.images.append(im)
    plt.show()
